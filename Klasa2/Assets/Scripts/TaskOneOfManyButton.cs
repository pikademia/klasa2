﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskOneOfManyButton : MonoBehaviour
{
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(() => GetTheAnswer());     
    }

    void GetTheAnswer()
    {
        if(GetComponent<Image>().sprite.name != "UISprite")
        {

            FindObjectOfType<TaskOneOfMany>().GetTheAnswerAfterButtonClick(GetComponent<Image>().sprite.name);
        }
        else
        {
            FindObjectOfType<TaskOneOfMany>().GetTheAnswerAfterButtonClick(GetComponentInChildren<Text>().text);
        }
    }
}
