﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class AdsManager : MonoBehaviour
{
    string gameId = "3629217";
    public string GameId { get { return gameId; } }

    string[] placementIDs = new string[] { "video", "rewardedVideo" };
    
    bool testMode = true;
    public bool TestMode { get { return testMode; } }

    public string GetPlacementId(int placementType)
    {
        return placementIDs[placementType];
    }
}
