﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckButton : MonoBehaviour
{
    [SerializeField] Image imgCircleIn;
    [SerializeField] Image imgSmallBg;
    [SerializeField] Image imgWithText;

    [SerializeField] Color32 colorOfBadAnswer;
    [SerializeField] Color32 colorOfGoodAnswer;
    bool canTextBeRotated;
    bool canBeResized;
    float resizeValue;
    float rotationValue;
    void Start()
    {
        canTextBeRotated = false;
        canBeResized = false;
    }

    private void Update()
    {
        RotateTheText();
        ResizeTheButton();
    }
    public void AnimateAnswer(bool isGoodAnswer)
    {
        if (!isGoodAnswer)
        {
            canTextBeRotated = true;
            StartCoroutine(ChangeColorByTime(colorOfBadAnswer, 0.6f));
            StartCoroutine(IncreaseTheSizeOfTheButton());
        }
        else
        {
            StartCoroutine(ChangeColorByTime(colorOfGoodAnswer, 0.6f));
            StartCoroutine(IncreaseTheSizeOfTheButton());
        }
    }

    IEnumerator ChangeColorByTime(Color32 newColor, float time)
    {
        imgCircleIn.color = newColor;
        imgSmallBg.color = newColor;
        yield return new WaitForSeconds(time);
        imgCircleIn.color = Color.white;
        imgSmallBg.color = Color.white;
  
    }

    IEnumerator IncreaseTheSizeOfTheButton()
    {
        rotationValue = 400f;
        resizeValue = 40f;
        canBeResized = true;
        yield return new WaitForSeconds(0.2f);
        rotationValue = -200f;
        resizeValue = -20f;
        yield return new WaitForSeconds(0.4f);
        canBeResized = false;
        canTextBeRotated = false;
    }

    void ResizeTheButton()
    {
        if (canBeResized)
        {
            GetComponent<RectTransform>().sizeDelta += new Vector2(resizeValue, resizeValue) * Time.deltaTime;
        }
    }

    void RotateTheText()
    {
        if (canTextBeRotated)
        {
            imgWithText.GetComponent<RectTransform>().Rotate(new Vector3(0f, 0f, rotationValue) * Time.deltaTime);
        }
        else
        {
            imgWithText.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0f, 0f, 0f);
        }
    }
}
