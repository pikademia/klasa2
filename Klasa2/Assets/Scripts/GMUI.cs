﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GMUI : MonoBehaviour
{
    public static GMUI instance;
    [SerializeField] GameObject LessonUI;
    [SerializeField] GameObject panelPopUp;
    [SerializeField] Text txtPopUp;
    [SerializeField] Button btnShowAnswer;
    [SerializeField] Text txtPoints;
    [SerializeField] Text txtLesson;
    [SerializeField] Text txtTask;
    [SerializeField] GameObject panelFinishLesson;
    [SerializeField] Slider sliderProgress;

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    public void UpdateTheSliderProgress()
    {
        sliderProgress.maxValue = LessonManager.instance.CurrentLessonTasksList.Length;
        sliderProgress.value = LessonManager.instance.GetTaskNumber();
    }

    private void Start()
    {
        ShowPopUpWindow(false);
    }
    public void GoToMenu()
    {
        LessonManager.instance.SaveTaskNumber();
        SceneManager.LoadScene((int)MyScenes.SceneMainMenu);
    }

    public void ShowLessonUI(bool isToBeActivated)
    {
        LessonUI.SetActive(isToBeActivated);
    }

    public void ShowPopUpWindow(bool isToBeActivated)
    {
        panelPopUp.SetActive(isToBeActivated);
    }
    public void ShowPopUpWindow(bool isToBeActivated, string textToBeDisplayed)
    {
        panelPopUp.SetActive(isToBeActivated);
        txtPopUp.text = textToBeDisplayed;
    }

    public void ShowButtonAnswer(bool isToBeActivated)
    {
        btnShowAnswer.gameObject.SetActive(isToBeActivated);
    }

    public void UpdateUIStats()
    {
        txtPoints.text = Saving.instance.GetPoints().ToString();
        txtLesson.text = LessonManager.instance.GetLessonNumber().ToString();
        txtTask.text = LessonManager.instance.GetTaskNumber().ToString();
    }

    public void OpenLessonFinishPanel(bool isToBeActivated)
    {
        panelFinishLesson.SetActive(isToBeActivated);
        if (!isToBeActivated)
        {
            GoToMenu();
        }
    }
}
