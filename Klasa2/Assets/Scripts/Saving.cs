﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saving : MonoBehaviour
{
    public static Saving instance;
    private void Awake()
    {
        PrepareSingleton();
        PreparePlayerPrefs();
    }

    void PrepareSingleton()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(gameObject);
    }
    void PreparePlayerPrefs()
    {
        if (!PlayerPrefs.HasKey("Points"))
        {
            PlayerPrefs.SetInt("Points", 0);
        }
        if (!PlayerPrefs.HasKey("ClassFinished"))
        {
            PlayerPrefs.SetInt("ClassFinished", 0);
        }
        if (!PlayerPrefs.HasKey("TaskNumber"))
        {
            PlayerPrefs.SetInt("TaskNumber", 0);
        }
    }

    public void SavePoints(bool isPointToBeAdded)
    {
        int points = PlayerPrefs.GetInt("Points");
        if (isPointToBeAdded)
        {
            points++;
        }
        else
        {
            points--;
        }
        PlayerPrefs.SetInt("Points", points);
    }
    
    public int GetPoints()
    {
        return PlayerPrefs.GetInt("Points");
    }

    public void SaveClassFinished()
    {
        int classFinished = PlayerPrefs.GetInt("ClassFinished");
        classFinished++;
        PlayerPrefs.SetInt("ClassFinished", classFinished);
    }

    public void SetLessonFinishedToMax(int maxLesson)
    {
        PlayerPrefs.SetInt("ClassFinished", maxLesson);
    }

    public int GetClassFinished()
    {
        return PlayerPrefs.GetInt("ClassFinished");
    }

    public void SaveTaskNumber(int taskNumber)
    {
        PlayerPrefs.SetInt("TaskNumber", taskNumber);
    }

    public int GetTaskNumber()
    {
        return PlayerPrefs.GetInt("TaskNumber");
    }

    public void ResetTaskNumber()
    {
        PlayerPrefs.SetInt("TaskNumber", 0);
    }


    public void ResetPlayerPrefs()
    {
        PlayerPrefs.SetInt("Points", 0);
        PlayerPrefs.SetInt("ClassFinished", 0);
        PlayerPrefs.SetInt("TaskNumber", 0);
    }
}
