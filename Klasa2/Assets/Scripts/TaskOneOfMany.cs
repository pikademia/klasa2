﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskOneOfMany : MonoBehaviour
{
    [SerializeField] Button[] btnAnswerButtons;
    [SerializeField] Text txtOneOfManyTask;
    [SerializeField] GameObject panelForButtons;
    [SerializeField] Image imgOneOfMany;
    [SerializeField] Sprite dumpSprite;
    bool isItTextTask;
    bool isItSpriteTask;
    int numberOfButtonsNeeded;
    string[] textAnswers;
    Sprite[] spriteAnswers;
    string userAnswer;
    string oryginalTaskQuestion;

    string goodAnswer;
    void Start()
    {
        userAnswer = "";
        oryginalTaskQuestion = LessonManager.instance.CurrentLessonTasksList[LessonManager.instance.GetTaskNumber()].GetOneOfManyTask();
        txtOneOfManyTask.text = oryginalTaskQuestion;
        AssignTaskImage();
        PrepareButtonQuantity();
        InsertUserInputToTheQuestion();
        GMUI.instance.ShowButtonAnswer(false);
    }

    public void GetTheAnswerAfterButtonClick(string answer)
    {
        userAnswer = answer;
        InsertUserInputToTheQuestion();
    }

    public void InsertUserInputToTheQuestion()
    {
        if (oryginalTaskQuestion.Contains("__"))
        {
            txtOneOfManyTask.text = oryginalTaskQuestion;
            if (userAnswer.Length > 0)
            {
                txtOneOfManyTask.text = txtOneOfManyTask.text.Replace("__", "<color=grey>" + userAnswer + "</color>");
            }
            else
            {
                txtOneOfManyTask.text = txtOneOfManyTask.text.Replace("__", "<color=red>" + " ? " + "</color>");
            }
        }
    }
    public void CheckTheAnswer()
    {
        if(goodAnswer == userAnswer)
        {
            LessonManager.instance.MarkTaskResult(true);
        }
        else
        {
            LessonManager.instance.MarkTaskResult(false);
        }
    }


    void AssignTaskImage()
    {
        if (LessonManager.instance.CurrentLessonTasksList[LessonManager.instance.GetTaskNumber()].GetOneOfManyTaskSprite() != null)
        {
            imgOneOfMany.sprite = LessonManager.instance.CurrentLessonTasksList[LessonManager.instance.GetTaskNumber()].GetOneOfManyTaskSprite();
            //imgOneOfMany.GetComponent<RectTransform>().sizeDelta = new Vector2(440f, 440f);
            //imgOneOfMany.GetComponent<RectTransform>().anchoredPosition = new Vector2(300f, -75f);
            //PositionQuestionTextField(450f);
            txtOneOfManyTask.GetComponent<RectTransform>().sizeDelta = new Vector2(1200f, 400f);
        }
        else
        {
            imgOneOfMany.sprite = null;
            txtOneOfManyTask.GetComponent<RectTransform>().sizeDelta = new Vector2(1850f, 400f);
            //imgOneOfMany.GetComponent<RectTransform>().sizeDelta = new Vector2(250f, 250f);
            //imgOneOfMany.GetComponent<RectTransform>().anchoredPosition = new Vector2(80f, -260f);
            //PositionQuestionTextField(640f);
        }
    }

    void PrepareButtonQuantity()
    {
        isItTextTask = false;
        isItSpriteTask = false;
        foreach ( Button btn in btnAnswerButtons)
        {
            btn.gameObject.SetActive(false);
        }
        textAnswers = LessonManager.instance.CurrentLessonTasksList[LessonManager.instance.GetTaskNumber()].GetOneOfManyTaskAnswers();
        spriteAnswers = LessonManager.instance.CurrentLessonTasksList[LessonManager.instance.GetTaskNumber()].GetOneOfManyTaskImageAnswers();
        if(textAnswers.Length > 1)
        {
            isItTextTask = true;
            numberOfButtonsNeeded = textAnswers.Length;
            for (int i = 0; i < numberOfButtonsNeeded; i++)
            {
                btnAnswerButtons[i].gameObject.SetActive(true);
            }
            goodAnswer = textAnswers[0];
            panelForButtons.GetComponent<GridLayoutGroup>().cellSize = new Vector2(460f, 180f);
            panelForButtons.GetComponent<GridLayoutGroup>().spacing = new Vector2(15f, 0f);
            AssignTextRandomValuesToTheButtons();
        }
        else if(spriteAnswers.Length > 1)
        {
            isItSpriteTask = true;
            numberOfButtonsNeeded = spriteAnswers.Length;
            for (int i = 0; i < numberOfButtonsNeeded; i++)
            {
                btnAnswerButtons[i].gameObject.SetActive(true);
                btnAnswerButtons[i].GetComponentInChildren<Text>().text = "";
            }
            goodAnswer = spriteAnswers[0].name;
            panelForButtons.GetComponent<GridLayoutGroup>().cellSize = new Vector2(300f, 300f);
            panelForButtons.GetComponent<GridLayoutGroup>().spacing = new Vector2(10f, 0f);
            AssignSpriteRandomValuesToTheButtons();
        }
        else
        {
            // no option available
            isItTextTask = false;
            isItSpriteTask = false;
        }
    }

    void AssignTextRandomValuesToTheButtons()
    {
        List<string> textAnswersTempList = new List<string>();
        for(int i = 0; i < numberOfButtonsNeeded; i++)
        {
            textAnswersTempList.Add(textAnswers[i]);
        }

        int j = numberOfButtonsNeeded;
        for(int i = 0; i < numberOfButtonsNeeded; i++)
        {
            int rand = Random.Range(0, j);
            btnAnswerButtons[i].GetComponentInChildren<Text>().text = textAnswersTempList[rand];
            textAnswersTempList.RemoveAt(rand);
            j--;
        }
    }
    void AssignSpriteRandomValuesToTheButtons()
    {
        List<Sprite> spriteAnswersTempList = new List<Sprite>();
        for (int i = 0; i < numberOfButtonsNeeded; i++)
        {
            spriteAnswersTempList.Add(spriteAnswers[i]);
        }

        int j = numberOfButtonsNeeded;
        for (int i = 0; i < numberOfButtonsNeeded; i++)
        {
            int rand = Random.Range(0, j);
            btnAnswerButtons[i].GetComponent<Image>().sprite = spriteAnswersTempList[rand];
            spriteAnswersTempList.RemoveAt(rand);
            j--;
        }
    }

}
