﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    [SerializeField] GameObject btnPrefab;
    [SerializeField] Transform prefabRodzic;
    [SerializeField] GameObject panelUstawien;
    int numberOfAllAvailableLessons = 6;
    void Start()
    {
        StworzPrzyciskiWyboryLekcji();
        GMUI.instance.ShowLessonUI(false);
        GMUI.instance.ShowButtonAnswer(false);
    }

    public void GetAccessToAllLessons()
    {
        Saving.instance.SetLessonFinishedToMax(numberOfAllAvailableLessons);
        GMUI.instance.GoToMenu();
    }

    void StworzPrzyciskiWyboryLekcji()
    {
        for(int i = 0; i < numberOfAllAvailableLessons; i++)
        {
            GameObject btn = Instantiate(btnPrefab, prefabRodzic);
            btn.GetComponentInChildren<Text>().text = (i + 1).ToString();
        }
    }

    public void OdkryjPanelUstawien(bool odkryj)
    {
        panelUstawien.SetActive(odkryj);
    }

    public void ResetAllProgress()
    {
        Saving.instance.ResetPlayerPrefs();
        OdkryjPanelUstawien(false);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void ZamknijProgram()
    {
        Application.Quit();
    }

    public void GoToPikademiaWebSite()
    {
        Application.OpenURL("http://pikademia.pl/");
    }
}
