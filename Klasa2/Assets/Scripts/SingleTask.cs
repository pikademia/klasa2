﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Lesson Task", menuName = "Create new lesson task")]
public class SingleTask : ScriptableObject
{
    [Header("Single Input Task")]
    [SerializeField] bool isThisSingleInputTask;
    [TextArea(3,5)]
    [SerializeField] string singleInputTask;
    [SerializeField] string singleInputTaskAnswer;
    [SerializeField] Sprite imgTask;


    [Header("One of Many Task")]
    [SerializeField] bool isThisOneOfManyTask;
    [TextArea(3, 5)]
    [SerializeField] string oneOfManyTask;
    [SerializeField] Sprite oneOfManyTaskSprite;
    [Tooltip("First answer should be the correct one")]
    [SerializeField] string[] oneOfManyTaskAnswers;
    [SerializeField] Sprite[] oneOfManyTaskImageAnswers;


    public bool IsThisSingleInputTask()
    {
        return isThisSingleInputTask;
    }

    public string GetSingleInputTask()
    {
        return singleInputTask;
    }

    public string GetSingleInputTaskAnswer()
    {
        return singleInputTaskAnswer;
    }

    public Sprite GetSingleInputTaskImage()
    {
        return imgTask;
    }

    // One of many tasks
    public bool IsThisOneOfManyTask()
    {
        return isThisOneOfManyTask;
    }
    public string GetOneOfManyTask()
    {
        return oneOfManyTask;
    }

    public Sprite GetOneOfManyTaskSprite()
    {
        return oneOfManyTaskSprite;
    }

    public string[] GetOneOfManyTaskAnswers()
    {
        return oneOfManyTaskAnswers;
    }

    public Sprite[] GetOneOfManyTaskImageAnswers()
    {
        return oneOfManyTaskImageAnswers;
    }
}
