﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskTypeInputAnswer : MonoBehaviour
{
    [SerializeField] Text txtTaskDescription;
    [SerializeField] InputField inpAnswer;
    [SerializeField] Image imgTask;
    string goodAnswer;
    string userAnswer;
    string oryginalTaskQuestion;
    void Start()
    {
        oryginalTaskQuestion = LessonManager.instance.CurrentLessonTasksList[LessonManager.instance.GetTaskNumber()].GetSingleInputTask();
        txtTaskDescription.text = oryginalTaskQuestion;
        goodAnswer = LessonManager.instance.CurrentLessonTasksList[LessonManager.instance.GetTaskNumber()].GetSingleInputTaskAnswer();
        GMUI.instance.ShowButtonAnswer(true);
        CheckForImage();
        userAnswer = "";
        CheckForUserInput();
    }

    void CheckForImage()
    {
        if (LessonManager.instance.CurrentLessonTasksList[LessonManager.instance.GetTaskNumber()].GetSingleInputTaskImage())
        {
            imgTask.sprite = LessonManager.instance.CurrentLessonTasksList[LessonManager.instance.GetTaskNumber()].GetSingleInputTaskImage();
            //txtTaskDescription.GetComponent<RectTransform>().sizeDelta = new Vector2(1160f, 500f);
        }
        else
        {
            //txtTaskDescription.GetComponent<RectTransform>().sizeDelta = new Vector2(1700f, 500f);
            imgTask.sprite = null;
        }
    }

    public void CheckForUserInput()
    {
        if (oryginalTaskQuestion.Contains("__"))
        {
            userAnswer = inpAnswer.text;
            txtTaskDescription.text = oryginalTaskQuestion.Replace("__", userAnswer);
        }
    }

    public void CheckTheAnswer()
    {
        if(inpAnswer.text.ToLower().Trim() == goodAnswer.ToLower())
        {
            LessonManager.instance.MarkTaskResult(true);
        }
        else
        {
            LessonManager.instance.MarkTaskResult(false);
            inpAnswer.text = "";
        }
    }
    public void ShowTheAnswer()
    {
        GMUI.instance.ShowPopUpWindow(true, "Poprawna odpowiedź to:\n" + goodAnswer);
    }
}
