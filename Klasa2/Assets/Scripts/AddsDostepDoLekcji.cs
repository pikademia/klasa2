﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class AddsDostepDoLekcji : MonoBehaviour, IUnityAdsListener
{
    Button myButton;
    AdsManager adsman;
    void Start()
    {
        myButton = GetComponent<Button>();
        adsman = FindObjectOfType<AdsManager>();
        myButton.interactable = Advertisement.IsReady(adsman.GetPlacementId(1));
        if (myButton) myButton.onClick.AddListener(ShowRewardedVideo);
        Advertisement.AddListener(this);
        Advertisement.Initialize(adsman.GameId, adsman.TestMode);
    }
    public void ShowRewardedVideo()
    {
        Advertisement.Show(adsman.GetPlacementId(1));
    }

    public void OnUnityAdsReady(string placementId)
    {
        if (placementId == adsman.GetPlacementId(1))
        {
            myButton.interactable = true;
        }
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        if (showResult == ShowResult.Finished)
        {
            FindObjectOfType<Menu>().GetAccessToAllLessons();
        }
        else if (showResult == ShowResult.Skipped) { }
        else if (showResult == ShowResult.Failed) { }
    }

    public void OnUnityAdsDidStart(string placementId) { }
    public void OnUnityAdsDidError(string message) { }
}
