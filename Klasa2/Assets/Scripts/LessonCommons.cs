﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LessonCommons : MonoBehaviour
{
    void Start()
    {
        GMUI.instance.ShowLessonUI(true);
        if (SceneManager.GetActiveScene().buildIndex == (int)MyScenes.SceneInputAnswer){
            GameObject.FindGameObjectWithTag("AnswerButton").GetComponent<Button>().onClick.AddListener(() => FindObjectOfType<TaskTypeInputAnswer>().ShowTheAnswer());
        }
        GMUI.instance.UpdateUIStats();
    }


}
