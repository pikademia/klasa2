﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LessonManager : MonoBehaviour
{
    public static LessonManager instance;
    private void Awake()
    {
        if(instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }

        DontDestroyOnLoad(gameObject);
    }
    [SerializeField] SingleTask[] Lesson1;
    [SerializeField] SingleTask[] Lesson2;
    [SerializeField] SingleTask[] Lesson3;
    [SerializeField] SingleTask[] Lesson4;
    [SerializeField] SingleTask[] Lesson5;
    [SerializeField] SingleTask[] Lesson6;
    public SingleTask[] CurrentLessonTasksList { get; set; }
    int taskNumber;
    int lessonNumber;
    SoundManager sManager;

    private void Start()
    {
        sManager = GetComponent<SoundManager>();
    }
    public void StartTheLesson(int lessonNumber_p)
    {
        lessonNumber = lessonNumber_p;
        if (lessonNumber - 1 == Saving.instance.GetClassFinished())
        {
            taskNumber = Saving.instance.GetTaskNumber();
        }
        else
        {
            taskNumber = 0;
        }
        SwitchTheRightTasksListOn();
        SwitchTheTask();
    }
    void SwitchTheRightTasksListOn()
    {
        switch (lessonNumber)
        {
            case 1:
                CurrentLessonTasksList = Lesson1;
                break;
            case 2:
                CurrentLessonTasksList = Lesson2;
                break;
            case 3:
                CurrentLessonTasksList = Lesson3;
                break;
            case 4:
                CurrentLessonTasksList = Lesson4;
                break;
            case 5:
                CurrentLessonTasksList = Lesson5;
                break;
            case 6:
                CurrentLessonTasksList = Lesson6;
                break;
        }
    }

    public void SwitchTheTask()
    {
        GMUI.instance.UpdateTheSliderProgress();
        ChooseSceneForTask(taskNumber);
    }

    void ChooseSceneForTask(int taskNumber_p)
    {
        if (CurrentLessonTasksList[taskNumber_p].IsThisSingleInputTask())
        {
            SceneManager.LoadScene((int)MyScenes.SceneInputAnswer);
        }
        else if (CurrentLessonTasksList[taskNumber_p].IsThisOneOfManyTask())
        {
            SceneManager.LoadScene((int)MyScenes.SceneOneOfMany);
        }
    }

    public void IncreaseTaskNumber()
    {
        taskNumber++;
        GMUI.instance.UpdateUIStats();
    }

    public void MarkTaskResult(bool isTheAnswerCorrect)
    {
        if (isTheAnswerCorrect)
        {
            AddPoint(true);
            sManager.OdtworzDzwiek(0);
            StartCoroutine(TasksToBeDoneAfterAnimationOfGoodAnswer());

            IEnumerator TasksToBeDoneAfterAnimationOfGoodAnswer()
            {
                FindObjectOfType<CheckButton>().AnimateAnswer(true);
                yield return new WaitForSeconds(0.6f);

                if (taskNumber < CurrentLessonTasksList.Length - 1)
                {
                    IncreaseTaskNumber();
                    SwitchTheTask();
                }
                else
                {
                    FinishLesson();
                }
            }
        }
        else
        {
            AddPoint(false);
            FindObjectOfType<CheckButton>().AnimateAnswer(false);
        }
    }

    public void FinishLesson()
    {
        taskNumber = 0;
        UnlockNextLesson();
        GMUI.instance.OpenLessonFinishPanel(true);
        sManager.OdtworzDzwiek(1);
    }

    public void UnlockNextLesson()
    {
        if(lessonNumber - 1 == Saving.instance.GetClassFinished())
        {
            Saving.instance.ResetTaskNumber();
            Saving.instance.SaveClassFinished();
        }
    }

    void AddPoint(bool isPointToBeAdded)
    {
        Saving.instance.SavePoints(isPointToBeAdded);
        GMUI.instance.UpdateUIStats();
    }

    public void SaveTaskNumber()
    {
        if(lessonNumber - 1 == Saving.instance.GetClassFinished())
        {
            Saving.instance.SaveTaskNumber(GetTaskNumber());
        }
    }
    public int GetTaskNumber()
    {
        return taskNumber;
    }

    public int GetLessonNumber()
    {
        return lessonNumber;
    }
}
