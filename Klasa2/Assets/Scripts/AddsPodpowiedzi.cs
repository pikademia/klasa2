﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class AddsPodpowiedzi : MonoBehaviour
{
    Button myButton;
    AdsManager adsman;

    void Start()
    {
        myButton = GetComponent<Button>();
        adsman = FindObjectOfType<AdsManager>();
        if (myButton) myButton.onClick.AddListener(ShowRewardedVideo);
        Advertisement.Initialize(adsman.GameId, adsman.TestMode);
    }
    public void ShowRewardedVideo()
    {
        Advertisement.Show(adsman.GetPlacementId(0));
        //FindObjectOfType<Lekcja>().OdkryjPanelPodpowiedzi(true);
    }

}
