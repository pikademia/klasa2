﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LessonPickButton : MonoBehaviour
{
    [SerializeField] Image imgPadlock;
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(() => SwitchLessonOn());
        if(System.Convert.ToInt32(GetComponentInChildren<Text>().text) > Saving.instance.GetClassFinished() + 1)
        {
            imgPadlock.gameObject.SetActive(true);
            GetComponent<Button>().interactable = false;
        }
        else
        {
            imgPadlock.gameObject.SetActive(false);
            GetComponent<Button>().interactable = true;
        }
    }

    void SwitchLessonOn()
    {
        int lessonNumber = System.Convert.ToInt32(GetComponentInChildren<Text>().text);
        LessonManager.instance.StartTheLesson(lessonNumber);
    }
}
