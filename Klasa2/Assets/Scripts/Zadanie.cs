﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName ="Zadanie", menuName = "Utwórz nowe zadanie")]
public class Zadanie : ScriptableObject
{
    [SerializeField] int typZadania;
    [TextArea(10, 100)]
    [SerializeField] string trescZadania;
    [SerializeField] string prawidlowaOdpowiedz;
    [SerializeField] string zlaOdpowiedz1;
    [SerializeField] string zlaOdpowiedz2;
    [SerializeField] string zlaOdpowiedz3;
    [SerializeField] Sprite zdjecieDoZadania;

    public Sprite PobierzZdjecieDoZadania()
    {
        return zdjecieDoZadania;
    }
    public string PobierzTrescZadania()
    {
        return trescZadania;
    }

    public string PobierzPrawidlowaOdpowiedz()
    {
        return prawidlowaOdpowiedz;
    }

    public string PobierzZlaOdpowiedz(int numerOdpowiedzi)
    {
        if(numerOdpowiedzi == 1)
        {
            return zlaOdpowiedz1;
        }else if(numerOdpowiedzi == 2)
        {
            return zlaOdpowiedz2;
        }else if(numerOdpowiedzi == 3)
        {
            return zlaOdpowiedz3;
        }
        else
        {
            return "blad";
        }
    }
}
